var mongoose = require('mongoose');

module.exports = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    invitation: {
        expiry: Date,
        token: String
    },
    created: Date,
    department: String,
    email: {
		type: String,
		required: true,
		unique: true
	},
    image: {
        base64: String,
        file_type: String
    },
    name: {
        first: String,
        last: String
    },
    password: {
        hash: String,
        reset: {
            expiry: Date,
            token: String
        }
    },
    phone: {
		area_code: Number,
		country_code: Number,
		number: Number
	},
    role: String,
	username: {
		type: String,
		required: true,
		unique: true
	}
});
