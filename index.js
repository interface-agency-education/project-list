if (process.env.NODE_ENV !== 'production') require('dotenv').config({ path: './../.env' });

var cors = require('cors')();
var projectSchema = require('./model');
var databaseConnection = require('@interface-agency/api-database-connection')('student');

module.exports.execute = function(request, response) {
	cors(request, response, function() {
		request = request || {};
		if (request.method === 'GET') {
			var Project = databaseConnection.model('Projects', projectSchema);
		    Project.find(function(err, results) {
		        if (err) return response.send(500, err);
		        return response.status(200).send(results);
		    });
		} else {
			return response.status(405);
		}
	});
}
